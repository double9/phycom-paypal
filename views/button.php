<?php

/**
 * @var \Phycom\Paypal\Module $module
 */

use Phycom\Paypal\Assets\PaypalAsset;

use yii\helpers\Url;
use yii\helpers\Html;


$bundle = PaypalAsset::register($this);

?>
<div class="payment-button-item">
	<form id="paypal-form" action="<?= $module->getServiceUrl(); ?>" method="post" target="_top">
		<input type="hidden" name="amount" value="">
		<input type="hidden" name="item_name" value="">
		<input type="hidden" name="custom" value="">

		<input type="hidden" name="return" value="<?= Yii::$app->getUrlManager()->createAbsoluteUrl("payment-return/?status=success"); ?>">
		<input type="hidden" name="cancel_return" value="<?= Url::canonical(); ?>">

		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="<?= $module->merchantId; ?>">
		<input type="hidden" name="button_subtype" value="services">
		<input type="hidden" name="no_note" value="0">
		<input type="hidden" name="no_shipping" value="1">
		<input type="hidden" name="currency_code" value="<?= $module->currency; ?>">

		<?php
			// TODO: Locale ei toeta eesti keelt
			// https://developer.paypal.com/docs/classic/api/locale_codes/

			/* <input type="hidden" name="lc" value="<?= str_replace("-", "_", Yii::$app->language) ?>"> */
		?>

		<?= Html::submitButton(
//                Html::img('https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_150x38.png', [
//                    'style' => 'height: 30px;'
//                ]),
                Html::img('https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg', [
                    'style' => 'height: 73px; position: relative; top: -16px;'
                ]),

				[
					'id' => 'paypal-payment-button',
					'class' => 'payment-button btn btn-flat btn-default'
				]
		); ?>
	</form>
</div>
