### Paypal payment gateway for Phycom platform

This Extension is used in conjunction with the Payment module

### Installation

Install using composer:

```
composer install phycom/paypal
```


### Configuration

Add payment method config under payment module like this:

```
modules => [
    'payment' => [
        'modules' => [
            \Phycom\Paypal\Module::ID => \Phycom\Paypal\Module::class
        ]
    ]
]
```
By default Paypal merchant id is fetched from environment
```
getenv(ENV . '_PAYPAL_MERCHANT_ID');
```
Where ``ENV`` is environment prefix defined in your app ``config.php``

If you would like to overload some default variables you can use array syntax like this:

```
modules => [
    'payment' => [
        'modules' => [
            \Phycom\Paypal\Module::ID => [
                'class' => \Phycom\Paypal\Module::class,
                'merchantId' => '<your paypal merchant id>',
                'currency' => 'EUR',
                'paymentInfo' => 'some text that appears under the payment button'
            ]
        ]
    ]
]
```

When payment method has been configured then settings box should appear at: ``<backend base path>/settings#t2-payment-methods``.
There you can also turn on or off the payment method.


### Adding the payment method to checkout page

If the theme has been configured properly it should appear by itself without no additional modifications.
Alternatively you can call ``\Phycom\Paypal\Module::getPaymentButtons()`` method by yourself where needed.
