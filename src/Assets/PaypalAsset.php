<?php

namespace Phycom\Paypal\Assets;

use Phycom\Base\Modules\Payment\Assets\PaymentAsset;

use yii\web\AssetBundle;

/**
 * Class PaypalAsset
 *
 * @package Phycom\Paypal\Assets
 */
class PaypalAsset extends AssetBundle
{
	public $sourcePath = '@vendor/phycom/paypal/assets';
	public $css = [
		'paypal.css'
	];
	public $js = [
		'paypal.js'
	];
	public $publishOptions = ['except' => ['*.less','*.scss']];
	public $depends = [
        PaymentAsset::class
	];
}
