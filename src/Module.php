<?php

namespace Phycom\Paypal;

use Phycom\Base\Components\MessageSource;
use Phycom\Base\Helpers\Currency;
use Phycom\Base\Models\Payment;
use Phycom\Base\Modules\Payment\Helpers\Reference;
use Phycom\Base\Modules\Payment\Interfaces\PaymentMethodInterface;
use Phycom\Base\Modules\Payment\Interfaces\TransactionInterface;
use Phycom\Base\Modules\Payment\Models\Transaction;
use Phycom\Base\Modules\Payment\Methods\PaymentMethod;

use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use Yii;

/**
 * Class Module
 *
 * @package Phycom\Paypal
 *
 * @property string $id
 * @property bool $isEnabled
 * @property string $name
 * @property string $label
 */
class Module extends PaymentMethod implements PaymentMethodInterface
{
    const ID = 'paypal';

    /**
     * @var string
     */
	public string $merchantId;
    /**
     * @var string
     */
	public string $currency = 'EUR';
    /**
     * @var string|null
     */
	public ?string $paymentInfo = null;


	public function init()
    {
        parent::init();

        if (!isset($this->merchantId)) {
            $this->merchantId = getenv(ENV . '_PAYPAL_MERCHANT_ID');
        }
        // do not use default view path
        if ($this->viewPath === $this->getBasePath() . DIRECTORY_SEPARATOR . 'views') {
            $this->setViewPath('@phycom/paypal/views');
        }

        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations['phycom/paypal*'])) {
            Yii::$app->i18n->translations['phycom/paypal*'] = [
                'class'    => MessageSource::class,
                'basePath' => '@phycom/paypal/translations',
                'catalog'  => false
            ];
        }
    }

    /**
     * @return string
     */
	public function getLogo() : string
	{
		return 'https://www.paypalobjects.com/webstatic/mktg/Logo/AM_mc_vs_ms_ae_UK.png';
	}

    /**
     * @return string
     */
	public function getServiceUrl() : string
    {
        return $this->testMode ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr';
    }

    /**
     * @return string
     */
    public function getIpnVerificationUrl() : string
    {
        return $this->testMode ? 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr' : 'https://ipnpb.paypal.com/cgi-bin/webscr';
    }

    /**
     * @return string|null
     */
	public function getPaymentInfo() : ?string
	{
		if ($this->paymentInfo === false) {
			return null;
		}
		if (strlen($this->paymentInfo)) {
			return $this->paymentInfo;
		}
		return Yii::t('phycom/paypal', 'Paypal and Credit Cards');
	}

    /**
     * @param array $params
     * @return string
     */
	public function getPaymentButtons(array $params = []) : string
	{
		return Yii::$app->view->renderFile(dirname(__FILE__) . '/views/button.php', ArrayHelper::merge(['module' => $this], $params));
	}

    /**
     * @param Payment $payment
     * @param string|null $methodName
     * @return TransactionInterface|Transaction|object
     * @throws InvalidConfigException
     */
	public function createTransaction(Payment $payment, string $methodName = null) : TransactionInterface
	{
        return Yii::createObject(Transaction::class, [[
            'amount'    => Currency::toDecimal($payment->amount),
            'reference' => Reference::create($payment)
        ]]);
	}
}
