<?php

namespace Phycom\Paypal\Controllers;

use Phycom\Paypal\Module as PaypalModule;

use Phycom\Base\Helpers\Date;
use Phycom\Base\Helpers\Currency;

use Phycom\Base\Models\Attributes\PaymentStatus;
use Phycom\Base\Models\Payment;

use Phycom\Base\Modules\Payment\Helpers\Reference;
use Phycom\Base\Modules\Payment\Helpers\Log;
use Phycom\Base\Modules\Payment\Exceptions\PaymentException;

use yii\base\Exception;
use Yii;

/**
 * Class NotifyController
 *
 * @package Phycom\Paypal\Controllers
 */
class NotifyController extends \yii\base\Controller
{
    const TRANSACTION_TYPE = 'web_accept';
    const PAYMENT_TYPE = 'instant';
	/**
	 * Response from PayPal indicating validation was successful
	 */
	const RES_VALID = 'VERIFIED';

	public $post = [];

	/**
	 * Verification Function
	 * Sends the incoming post data back to PayPal using the cURL library.
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function verifyIPN()
	{
		if (empty(Yii::$app->request->post())) {
			throw new PaymentException("No post data");
		}

		$raw_post_data = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);

		foreach ($raw_post_array as $keyval) {
			$keyval = explode('=', $keyval);
			if (count($keyval) == 2) {
				// Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
				if ($keyval [0] === 'payment_date') {
					if (substr_count($keyval [1], '+') === 1) {
						$keyval[1] = str_replace('+', '%2B', $keyval [1]);
					}
				}
				$this->post[$keyval[0]] = urldecode($keyval [1]);
			}
		}

		// Build the body of the verification post request, adding the _notify-validate command.
		$req = 'cmd=_notify-validate';
		$get_magic_quotes_exists = false;
		if (function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		foreach ($this->post as $key => $value) {
			if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}

		// Post the data back to PayPal, using curl. Throw exceptions if errors occur.
		$ch = curl_init(PaypalModule::getInstance()->getIpnVerificationUrl());
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		$res = curl_exec($ch);

		if (!($res)) {
			$errNo = curl_errno($ch);
			$errStr = curl_error($ch);
			curl_close($ch);
			throw new PaymentException('Paypal curl error: ' . $errNo . ' - ' . $errStr);
		}

		$info = curl_getinfo($ch);
		$httpCode = $info ['http_code'];
		if ($httpCode != 200) {
			throw new PaymentException('Invalid paypal HTTP response code ' . $httpCode);
		}
		curl_close($ch);

		// Check if PayPal verifies the IPN data, and if so, return true.
		if ($res == self::RES_VALID) {
			return true;
		} else {
			return false;
		}
	}

    /**
     * @throws PaymentException
     * @throws \Throwable
     */
	public function actionIpn()
	{
        Log::info('Paypal payment notify');
        Log::info(Yii::$app->request->post());
        Log::info(Yii::$app->request->rawBody);

		/**
		 * Sample POST:
		 *
		 * array (
		 * 'transaction_subject' => '',
		 * 'payment_date' => '11:05:29 Jul 24, 2017 PDT',
		 * 'txn_type' => 'web_accept',
		 * 'last_name' => 'Kalda',
		 * 'residence_country' => 'KH',
		 * 'item_name' => 'Order #8302388228',
		 * 'payment_gross' => '',
		 * 'mc_currency' => 'EUR',
		 * 'business' => 'taivo.kuusik@gmail.com',
		 * 'payment_type' => 'instant',
		 * 'protection_eligibility' => 'Eligible',
		 * 'verify_sign' => 'AYrfI31D-kkncvntlw-bRzr.fIddANyWjKhY8o3TZLesXUW83uTOmgpu',
		 * 'payer_status' => 'unverified',
		 * 'payer_email' => 'kalda.kaido@gmail.com',
		 * 'txn_id' => '9XG41833EK7868459',
		 * 'quantity' => '1',
		 * 'receiver_email' => 'taivo.kuusik@gmail.com',
		 * 'first_name' => 'Kaido',
		 * 'payer_id' => '8BPFNGMUWMEE8',
		 * 'receiver_id' => 'YCADM92CJ82V6',
		 * 'item_number' => '',
		 * 'payment_status' => 'Completed',
		 * 'payment_fee' => '',
		 * 'mc_fee' => '0.10',
		 * 'mc_gross' => '0.10',
		 * 'custom' => '8302388228',
		 * 'charset' => 'windows-1252',
		 * 'notify_version' => '3.8',
		 * 'ipn_track_id' => '9828cff5494ce',
		 * )
		 */

		try {
			if ($this->verifyIPN()) {

			    if (Yii::$app->request->post('txn_type') !== self::TRANSACTION_TYPE) {
                    throw new Exception('Invalid paypal IPN transaction type ' . Yii::$app->request->post('txn_type'));
                }

                if (Yii::$app->request->post('payment_type') !== self::PAYMENT_TYPE) {
                    throw new Exception('Invalid paypal IPN payment type ' . Yii::$app->request->post('payment_type'));
                }

                $payment = Reference::parse(Yii::$app->request->post('custom'));
                $this->updatePayment($payment, PaymentStatus::COMPLETED, Yii::$app->request->post());

                $invoice = $payment->invoice;
                if ($invoice->totalPaid < $invoice->total) {
                    Log::warning('Partial Paypal payment ' . $payment->transaction_id);
                }
                Log::info('Paypal transaction ' . $payment->transaction_id . ' complete');

			} else {
				throw new Exception('Invalid paypal IPN response');
			}
			header("HTTP/1.1 200 OK");

		} catch (\Exception $e) {
			throw new PaymentException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}

	/**
	 * @param Payment $payment
	 * @param $status
	 * @param $transactionData
	 * @return Payment
	 * @throws PaymentException
	 */
	protected function updatePayment(Payment $payment, $status, $transactionData) : Payment
	{
		$payment->status = $status;
		$payment->transaction_id = $transactionData['txn_id'];
		$payment->transaction_data = $transactionData;
		$payment->transaction_time = Date::create($transactionData["payment_date"])->dateTime;
		$payment->amount = Currency::toInteger($transactionData['mc_gross']);

		if (isset($transactionData['charset']) && strtolower($transactionData['charset']) !== 'utf-8') {
            $payment->remitter = mb_convert_encoding($transactionData['first_name'], 'UTF-8', $transactionData['charset']) . ' ' . mb_convert_encoding($transactionData['last_name'], 'UTF-8', $transactionData['charset']);
        } else {
            $payment->remitter = $transactionData['first_name'] . ' ' . $transactionData['last_name'];
        }
		if (!$payment->save()) {
			throw new PaymentException('Error saving payment: ' . json_encode($payment->errors));
		}
		return $payment;
	}
}
