jQuery(function($) {
    $("#paypal-payment-button:not(.disabled)").click(function(e) {
        e.preventDefault();
        window.submitPayment('paypal', this).done(function (data) {
            var $paymentForm = $("#paypal-form");
            if ($paymentForm.length) {
                console.log("Writing data: ", data, " to ", $paymentForm);
                $paymentForm.find('input[name="amount"]').val(data.amount);
                $paymentForm.find('input[name="item_name"]').val("Order #" + data.order);
                $paymentForm.find('input[name="custom"]').val(data.reference);
                $paymentForm.submit();
            }
        });
    });
});